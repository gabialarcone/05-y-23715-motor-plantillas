const HomeController = require('../controllers/admin/home.controller');

const inicioController = {
    index: (req, res) => {
        res.render("admin/index");
    }
}

module.exports = inicioController;