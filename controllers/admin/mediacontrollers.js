const { getAll, insert, getLastOrder, getLastId, runQuery, run, updateMedia } = require('../../db/conexion');
const multer = require('multer');




// Configurar multer para la carga de archivos
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/images/uploads/');
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + path.extname(file.originalname));
    }
});

const upload = multer({ storage: storage });
const MediaController = {
    //Definición del objeto
    //index - listado
    index: async function (req, res){
        try {
            const rows = await getAll('SELECT m.*, i.nombre AS integranteNombre FROM media m LEFT JOIN integrante i ON m.matricula = i.matricula WHERE m.activo  = 1 AND i.activo = 1');
            console.log('Media rows:', rows);
            res.render('admin/media/index', {
                media: rows,
            });
        } catch (error) {
            console.error('Error al listar media:', error);
            res.status(500).send('Error al listar media');
        }
    },

    //create - formulario de creación
    create: async function (req, res){
        try {
            const integrantes = await getAll('SELECT * FROM integrante');
            const tiposMedia = await getAll('SELECT * FROM tipomedia'); // Get all media types
            res.render('admin/media/crearMedia', { integrantes, tiposMedia });
        } catch (error) {
            console.error('Error al cargar los datos necesarios:', error);
            res.status(500).send('Hubo un error al cargar los datos necesarios. Por favor, inténtelo de nuevo.');
        }

    },

    //store - método de guardar en la base de datos
    store: async function (req, res) {
        try {
            const { url, titulo, id_tmedia, matricula, activo } = req.body;
            let newPath = '';

            if (req.file) {
                newPath = `/images/uploads/${req.file.filename}`;
            }

            if (!newPath && !url) {
                return res.status(400).json({ error: 'Debe proporcionar una URL o cargar una imagen.' });
            }

            const ultimoOrden = await getLastOrder('media');
            const nuevoOrden = ultimoOrden + 1;

            const query = `
                INSERT INTO media (src, url, titulo, id_tmedia, matricula, activo, nro_orden)
                VALUES (?, ?, ?, ?, ?, ?, ?)
            `;
            const params = [newPath, url, titulo, id_tmedia, matricula, activo, nuevoOrden];
            await runQuery(query, params);

            res.redirect(`/admin/media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
        } catch (error) {
            console.error('Error al crear media:', error);
            res.status(500).json({ error: 'Error al crear media' });
        }
    },

    //show - formulario de ver un registro
    show: async function (req, res) {
        try {
            const id = req.params.id;
            const media = await getAll('SELECT * FROM media WHERE id = ?', [id]);
            if (media.length === 0) {
                return res.status(404).json({ error: 'Media no encontrado' });
            }
            res.json(media[0]);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Hubo un error al cargar el  media. Por favor, inténtelo de nuevo.' });
        }
    },

    //destroy - operación de eliminar un registro
    async destroy(req, res) {
        const id = req.params.id;
        console.log("id", id);

        try {
            const sql = 'UPDATE media SET activo = 0 WHERE id = ?';
            await runQuery(sql, [id]);
            res.redirect('/admin/media/listar');
        } catch (error) {
            console.error('Error al actualizar el estado del integrante:', error);
            res.status(500).json({ message: 'Error al actualizar el estado del integrante' });
        }
    },
    //edit - formulario de edición
    edit: async function (req, res) {
        try {
            const id = req.params.id;
            const [media] = await getAll('SELECT * FROM media WHERE id = ?', [id]);
            const integrantes = await getAll('SELECT matricula, nombre, apellido FROM integrante');
            const tiposMedia = await getAll('SELECT id_tmedia, nombre FROM tipomedia');

            if (!media) {
                return res.status(404).send('Media no encontrado');
            }

            // Añadir flags para los valores seleccionados
            integrantes.forEach(integrante => {
                integrante.selected = integrante.matricula === media.matricula;
            });

            tiposMedia.forEach(tipo => {
                tipo.selected = tipo.id_tmedia === media.id_tmedia;
            });

            res.render('admin/media/editMedia', {
                media,
                integrantes,
                tiposMedia
            });
        } catch (error) {
            console.error('Error al obtener el media:', error);
            res.status(500).send('Error al obtener el media');
        }
    },


    //update - método de editar un registro

// Manejar la solicitud de actualización de media
    update: async (req, res) => {
        try {
            const id = req.params.id;
            console.log("ID:", id);
            console.log("Editar body:", req.body);

            const { url: bodyUrl, titulo, id_tmedia, matricula, activo, src } = req.body;
            let newPath = '';

            if (req.file) {
                newPath = `/images/uploads/${req.file.filename}`;
            }
            console.log("New path:", newPath);
            console.log("URL:", bodyUrl);

            // Asegúrate de que al menos uno de newPath o bodyUrl esté presente
            if (!newPath && !bodyUrl) {
                return res.status(400).json({ error: 'Debe proporcionar una URL o cargar una imagen.' });
            }

            // Construir la consulta SQL dinámicamente
            const updates = [];
            const params = [];

            if (newPath) {
                updates.push("src = ?");
                params.push(newPath);
            }
            if (bodyUrl) {
                updates.push("url = ?");
                params.push(bodyUrl);
            }

            updates.push("titulo = ?");
            params.push(titulo);

            updates.push("id_tmedia = ?");
            params.push(id_tmedia);

            updates.push("matricula = ?");
            params.push(matricula);

            updates.push("activo = ?");
            params.push(activo);

            params.push(id);

            const sql = `UPDATE media SET ${updates.join(', ')} WHERE id = ?`;
            console.log("SQL:", sql);
            console.log("Params:", params);

            await runQuery(sql, params);

            res.redirect(`/admin/media/listar?success=${encodeURIComponent('¡Registro editado correctamente!')}`);
        } catch (error) {
            console.error('Error al editar media:', error);
            res.status(500).json({ error: 'Error al editar media' });
        }
    }









};




module.exports = MediaController;