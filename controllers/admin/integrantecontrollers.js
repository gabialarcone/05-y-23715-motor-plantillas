const { run,getAll, insert, getLastOrder,  runQuery, db} = require('../../db/conexion');

const IntegrantesController = {
    //Definición del objeto
    //index - listado
    index: async function (req, res){
        const rows = await getAll("select * from integrante where activo = 1");
        //console.log(rows);
        res.render("admin/integrante/index", {
            integrantes: rows,
        });
    },

    //create - formulario de creación
    create: function (req, res) {
        res.render("admin/integrante/crearForm");
    },

    //store - método de guardar en la base de datos
    store: async function (req, res) {
        try {
            const lastOrder = await getLastOrder("integrante");
            const newOrder = lastOrder + 1;
            const { nombre, apellido, matricula, pagina, activo } = req.body;


            // Validar los datos

            const errors = [];
            if (!matricula || typeof matricula !== 'string' || matricula.trim().length < 3 || !/^[a-zA-Z0-9]+$/.test(matricula)) {
                errors.push('Matricula inválida. Debe tener al menos 3 caracteres alfanuméricos.');
            }
            if (!nombre || typeof nombre !== 'string' || nombre.trim().length < 2 || !/^[a-zA-Z]+$/.test(nombre)) {
                errors.push('Nombre inválido. Debe tener al menos 2 letras.');
            }
            if (!apellido || typeof apellido !== 'string' || apellido.trim().length < 2 || !/^[a-zA-Z]+$/.test(apellido)) {
                errors.push('Apellido inválido. Debe tener al menos 2 letras.');
            }


            if (errors.length > 0) {
                return res.status(400).render('admin/integrante/crearForm', {
                    errors,
                    data: req.body,
                });
            }

            // Insertar los datos en la tabla integrantes
            await insert('INSERT INTO integrante (nro_orden, matricula, nombre, apellido, pagina, activo) VALUES (?, ?, ?, ?, ?, ?)',
                [newOrder, matricula, nombre, apellido, pagina, activo]);
            // Redireccionar al usuario al listado de integrantes después de una inserción exitosa
            res.redirect(`/admin/integrante/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
        } catch (error) {
            console.error(error);
            res.redirect(`/admin/integrante/create?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
        }
    },

    //show - formulario de ver un registro
    show: async function (req, res) {
        try {
            const matricula = req.params.matricula;
            const integrante = await getAll('SELECT * FROM integrante WHERE matricula = ?', [matricula]);
            if (integrante.length === 0) {
                return res.status(404).json({ error: 'Integrante no encontrado' });
            }
            res.json(integrante[0]);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Hubo un error al cargar el integrante. Por favor, inténtelo de nuevo.' });
        }
    },



    //destroy - operación de eliminar un registro
    destroy: async function (req, res) {
        const matricula = req.params.matricula;
       console.log("matricula", matricula);

        try {
            const sql = 'UPDATE integrante SET activo = 0 WHERE matricula = ?';
            await runQuery(sql, [matricula]);
            res.redirect('/admin/integrante/listar');
        } catch (error) {
            console.error('Error al actualizar el estado del integrante:', error);
            res.status(500).json({ message: 'Error al actualizar el estado del integrante' });
        }
    },

    //edit - formulario de edición
    edit: async function (req, res) {
        try {
            const matricula = req.params.matricula;
            const integrante = await getAll('SELECT * FROM integrante WHERE matricula = ?', [matricula]);
            if (!integrante) {
                return res.status(404).send('Integrante no encontrado');
            }
            res.render('admin/integrante/editForm', {
                integrante: integrante
            });
        } catch (error) {
            console.error('Error al obtener el integrante:', error);
            res.status(500).send('Error al obtener el integrante');
        }
    },

    async update(req, res) {
        const matricula = req.params.matricula;
        const { nombre, apellido } = req.body; // Obtén los datos del cuerpo de la solicitud
        console.log("matricula", matricula);

        try {
            const sql = 'UPDATE integrante SET nombre = ?, apellido = ? WHERE matricula = ?';
            await runQuery(sql, [nombre, apellido, matricula]);
            res.redirect('/admin/integrante/listar');
        } catch (error) {
            console.error('Error al actualizar el integrante:', error);
            res.status(500).json({ message: 'Error al actualizar el integrante' });
        }
    }


};




module.exports = IntegrantesController;