const { getAll, insert, getLastOrder, getLastId, runQuery, run} = require('../../db/conexion');

const TipoMediaController = {
    //Definición del objeto
    //index - listado
    index: async function (req, res){
        const rows = await getAll("SELECT * FROM tipomedia WHERE activo = 1");
        console.log(rows);
        res.render("admin/tipomedia/index", {
            tipomedia: rows,
        });
    },

    //create - formulario de creación
    create: function (req, res){
        res.render('admin/tipomedia/crearTipoMedia');

    },

    //store - método de guardar en la base de datos
    store: async function (req, res) {
        try {
            const ultimoOrden = await getLastOrder("tipomedia");
            const nuevoOrden = ultimoOrden + 1;
            const ultimoId = await getLastId("tipomedia");
            const nuevoId = ultimoId + 1;

            const { nombre, activo } = req.body;
            const isActive = activo ? 1 : 0;

            if (!nombre) {
                return res.status(400).json({ error: 'Debes completar todos los campos.' });
            }

            await insert('INSERT INTO tipomedia (nro_orden, id_tmedia, nombre, activo) VALUES (?, ?, ?, ?)',
                [nuevoOrden, nuevoId, nombre, isActive]);

            res.redirect(`/admin/tipomedia/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
        } catch (error) {
            console.error('Error al crear tipo media:', error);
            res.status(500).json({ error: 'Error al crear tipo media' });
        }
    },

    //show - formulario de ver un registor
    show: async function (req, res) {
        try {
            const id_tmedia = req.params.id_tmedia;
            const tipomedia = await getAll('SELECT * FROM tipomedia WHERE id_tmedia = ?', [id_tmedia]);
            if (tipomedia.length === 0) {
                return res.status(404).json({ error: 'Tipo de media no encontrado' });
            }
            res.json(tipomedia[0]);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Hubo un error al cargar el tipo de media. Por favor, inténtelo de nuevo.' });
        }
    },


    //destroy - operación de eliminar un registro
    async destroy(req, res) {
        const id_tmedia = req.params.id_tmedia;
        console.log("id_tmedia", id_tmedia);

        try {
            const sql = 'UPDATE tipomedia SET activo = 0 WHERE id_tmedia = ?';
            await runQuery(sql, [id_tmedia]);
            res.redirect('/admin/tipomedia/listar');
        } catch (error) {
            console.error('Error al actualizar el estado del media:', error);
            res.status(500).json({ message: 'Error al actualizar el estado del media' });
        }
    },

    //edit - formulario de edición
    edit: async function (req, res) {
        try {
            const id_tmedia = req.params.id_tmedia;
            const tipomedia = await getAll('SELECT * FROM tipomedia WHERE id_tmedia = ?', [id_tmedia]);
            if (!tipomedia) {
                return res.status(404).send('Tipo media no encontrado');
            }
            res.render('admin/tipomedia/editTipoMedia', {
                tipomedia : tipomedia
            });
        } catch (error) {
            console.error('Error al obtener el integrante:', error);
            res.status(500).send('Error al obtener el integrante');
        }
    },

    //update - método de editar un registro
    update: async function (req, res) {
        const id_tmedia = req.params.id_tmedia;
        if (!req.body.nombre ) {
            return res.status(400).json({error: 'Debes completar todos los campos.'});
        }
        try {
            await run("UPDATE tipomedia SET nombre = ? WHERE id_tmedia = ?",
                [
                    req.body.nombre,
                    id_tmedia
                ]);
            res.redirect(`/admin/tipomedia/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
        } catch (error) {
            console.error('Error al actualizar el integrante:', error);
            res.status(500).json({error: '¡Error al actualizar al integrante!'});
        }
    }


};



module.exports = TipoMediaController;