const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const dbPath = path.resolve(__dirname, 'integrantes.sqlite');
const db = new sqlite3.Database(dbPath);


db.serialize(() => {
    db.run('SELECT 1', [], (err) => {
        if (err) {
            console.error('Error al conectar con la base de datos:', err);
        } else {
            console.log('Conexión exitosa con la base de datos.');
            //db.run("select * from integrante");
        }
    });
});

async function getAll(query, parametros){
    return new Promise((resolve, reject)=>{
        db.all(query, parametros, (error, rows)=>{
            if (error) {
                reject(error);
            } else{
                resolve(rows);
            }
        });
    });
}

async function getLastOrder(){
    const result = await getAll("select  Max(nro_orden) as maxOrden from integrante");
    return result[0].maxOrden;
}

async function insert(query, parametros) {
    return new Promise((resolve, reject) => {
        db.run(query, parametros, function(error) {
            if (error) {
                reject(error);
            } else {
                resolve(this.lastID);
            }
        });
    });
}

async function getLastId(tipomedia) {
    const result = await getAll(`SELECT MAX(id_tmedia) as maxId FROM ${tipomedia}`);
    return result[0].maxId;
}


function runQuery(query, params) {
    return new Promise((resolve, reject) => {
        db.run(query, params, function (err) {
            if (err) {
                return reject(err);
            }
            resolve(this.lastID); // Devuelve el ID de la última fila insertada
        });
    });
}

async function run(query, params) {
    return new Promise((resolve, reject) => {
        db.run(query, params, function (error) {
            if (error) {
                reject(error);
            } else {
                resolve();
            }
        })
    })
}

const updateMedia = async (id, titulo, id_tmedia, matricula, newPath, url) => {
    try {
        // Construir la consulta de actualización
        let query = `
            UPDATE media 
            SET titulo = ?,
                id_tmedia = ?,
                matricula = ?,
                ${newPath ? 'src = ?,' : ''}
                ${url ? 'url = ?,' : ''}
                id = id
            WHERE id = ?
        `;

        // Crear un array con los parámetros de la consulta
        let params = [titulo, id_tmedia, matricula];
        if (newPath) params.push(newPath);
        if (url) params.push(url);
        params.push(id);

        // Ejecutar la consulta SQL
        await runQuery(query, params);

        // La actualización se realizó correctamente
        return true;
    } catch (error) {
        // Manejar cualquier error que ocurra durante la ejecución de la consulta
        console.error('Error al actualizar media:', error);
        throw error; // Lanza el error para que sea manejado por el código que llama a esta función
    }
};

module.exports = {
    db,
    getAll,
    insert,
    getLastOrder,
    getLastId,
    runQuery,
    run,
    updateMedia,

}

